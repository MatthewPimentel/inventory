/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventory;

/**
 *
 * @author Matthew
 */
public class Inventory {
    private Item item;
    
    public Inventory(Item item){
        this.item = item;
    }
     
    public void setItem(Item item){
        this.item = item;
    }
    
    public Item getItem(){
        return item;
    }
   
    public String toString(){
        return "item: " + item;
    }
    
}
