package inventory;

import java.util.*;

/**
 * Simulator to add items, print inventory and to search item's quantity.
 *
 * @author Matthew Pimentel
 */
public class InventorySystem {

    public static void main(String[] args) {
        ArrayList<Inventory> inventory = new ArrayList<Inventory>();
     
        
        Item item1 = new Item(1,350,"Phones");
        Inventory i1 = new Inventory(item1);
        inventory.add(i1);
        /**
         * Add a new item to the inventory. Program asks the user to enter name
         * and quantity of the item. For new item's ID, program simply
         * increments the ID of the last item added.
         */
        Scanner input = new Scanner(System.in);
        
        System.out.println("Enter name of the item to add:");
        String name = input.nextLine();
        
        System.out.println("Enter the quantity:");
        int quantity = input.nextInt();
        
        Item item2 = new Item(2, quantity, name);
        Inventory i2 = new Inventory(item2);
        inventory.add(i2);
        
        

        /**
         * Add two more items.
         */
        Item item3 = new Item(3, 5, "ET-2750 printer");
        Inventory i3 = new Inventory(item3);
        inventory.add(i3);
        

        Item item4 = new Item(4, 10, "MX-34 laptops");
        Inventory i4 = new Inventory(item4);
        inventory.add(i4);
        /**
         * Print the inventory.
         */
        for (int i =0; i < inventory.size(); i++){
            System.out.println(inventory.get(i));
        }

        /**
         * Find quantity of a particular item in the inventory.
         */
        System.out.println("Enter ID of the item whose quantity you want to find:");
        int temp_ID = input.nextInt();
        System.out.println("Item's quantity is: " + inventory.get(temp_ID - 1));

    }

}
