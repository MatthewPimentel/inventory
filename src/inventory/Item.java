package inventory;

/**
 * A class to model items stored in the inventory of a Warehouse. For simplicity, let's assume each item in the
 * inventory has a name, quantity and a unique ID. The system allows to add new items, find quantity by item's ID and
 * print full inventory. Use this code to answer Q#16 and Q#17 of Part B.
 *
 * @author Matthew Pimentel
 */
public class Item {

    private int itemID;
    private String name;
    private int quantity;
    /**
     * An array to hold up to 100 items.
     */


    /**
     * Constructor takes in item's ID, quantity and it's name.
     *
     * @param itemID Unique ID of the item.
     * @param quantity the quantity of the item.
     * @param name the name of the item.
     */
    public Item(int itemID, int quantity, String name) {
        this.itemID = itemID;
        this.quantity = quantity;
        this.name = name;
    }


    public void setItemID(int itemID){
        this.itemID = itemID;
    }
    
    public int getItemID(){
        return itemID;
    }
    
    public void setQuantity(int quantity){
        this.quantity = quantity;
    }
    
    public int getQuantity(){
        return quantity;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getName(){
        return name;
    }
    
    public String toString(){
        return "Name: " + name + " ID: " + itemID + " Quantity:  " + quantity;
    }

}
